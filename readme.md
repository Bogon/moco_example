# Moco 模拟服务端

[Moco](https://github.com/dreamhead/moco) 是一个可以轻松搭建测试服务器的工具。

## 为什么需要模拟服务端

作为一个移动开发人员，有时由于服务端开发进度慢，空有一个iPhone应用但发挥不出作用。幸好有了 Moco，只需配置一下请求和返回数据，很快就可以搭建一个模拟服务，无需等待服务端开发完成才能继续开发。当服务端完成后，修改访问地址即可。

有时服务端 API 应该是什么样子都还没清楚，由于有了 Moco 模拟服务，在开发过程中，可以不断调整API设计，搞清楚真正自己想要的 API 是什么样子的。就这样，在服务端代码还没真正动手之前，已经提供一份真正满足自己需要的API文档，剩下的就交给服务端照着API去实现就行了。

还有一种情况就是，服务端已经写好了，剩下客户端还没完成。由于 Moco 是本地服务，访问速度比较快，所以通过使用 Moco 来模拟服务端，这样不仅可以提高客户端的访问速度，还提高网络层测试代码访问速度的稳定性。

## 如何使用 Moco 模拟服务

### 安装

如果你是使用 Mac 或 Linux，可以尝试一下步骤：

1. 确定你安装 JDK 6 以上
2. 下载脚本
3. 设置它可以执行(chmod 755 ~/bin/moco)

编写配置文件 `foo.json`，内容如下：

```javascript
[
   {
     "response" :
       {
         "text" : "Hello, Moco"
       }
   }
]
```

### 运行 Moco HTTP 服务器

```bash
moco start -p 12306 -c foo.json
```

打开浏览器访问 `http://localhost:12306`，你会看见 `"Hello, Moco"`

### 配置服务

由于有时候服务端返回的数据比较多，所以将服务端响应的数据独立在一个 JSON 文件中。以登陆为例，将数据存放在 `login_response.json`

```javascript
{
    "access_token": "4422ea7f05750e93a101cb77ff76dffd3d65d46ebf6ed5b94d211e5d9b3b80bc",
    "token_type": "bearer",
    "scope": "user",
    "created_at": 1428040414
}
```

而将请求 uri 路径，方法 ( method )和参数 ( queries )等配置放在  `login_conf.json` 文件中

```javascript
[
  {
    "request" :
      {
        "uri" : "/oauth/token",
        "method" : "post",
        "queries" : 
          {
            "grant_type" : "password",
            "username" : "liuyaozhu13hao@163.com",
            "password" : "freedom13",
            "client_secret" : "53e3822c49287190768e009a8f8e55d09041c5bf26d0ef982693f215c72d87da",
            "client_id" : "750ab22aac78be1c6d4bbe584f0e3477064f646720f327c5464bc127100a1a6d"
          }
      },
    "response" :
      {
        "file" : "./Login/login_response.json"
      }
  }
]
```

上面的配置文件中， uri 路径使用了相对路径。想更加详细了解如何配置，请查阅官网的 HTTP(s) APIs。

还有一个需要配置地方就是，由于实际开发中肯定不止一个客户端请求，所以还需要一个配置文件 `settings.json` 来包含很有的请求。

```javascript
[
    {
        "include" : "./Story/stories_conf.json"
    },
    {
        "include" : "./Login/login_conf.json"
    },
    {
        "include" : "./Story/story_upvote_conf.json"
    }
]
```

### 启动服务

将路径跳转到 moco 所在的目录，找到 `settings.json` 文件，使用命令行来启动服务：

```bash
java -jar moco-runner-0.10.2-standalone.jar http -p 8080 -c config.json
```